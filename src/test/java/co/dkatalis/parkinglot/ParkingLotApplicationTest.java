package co.dkatalis.parkinglot;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ParkingLotApplicationTest {

    private final ParkingLotApplication parkingLot = new ParkingLotApplication();

    @Test
    @DisplayName("readInput() - Given input - Should capture input")
    void readInput_givenInput_shouldCaptureInput() {
        // Given
        String expected = UUID.randomUUID().toString();
        InputStream inputStream = new ByteArrayInputStream(expected.getBytes(StandardCharsets.UTF_8));
        System.setIn(inputStream);

        // When
        String actual = parkingLot.readInput();

        // Then
        assertEquals(expected, actual);
    }
}
